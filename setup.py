from setuptools import setup, find_packages


setup(
    name='zanate',
    version='0.1',
    description='Simple static site generator',
    long_description='',
    author='Adolfo Fitoria',
    author_email='adolfo@fitoria.net',
    packages=find_packages(),
    install_requires=['jinja2', 'manage.py'],
    entry_points={
        'console_scripts': [
            'zanate = zanate:main',
        ]
    },
    classifiers=[
        'Development Status :: 1 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
)
