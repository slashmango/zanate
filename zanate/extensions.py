from jinja2 import nodes
from jinja2.ext import Extension

class PhotoPlaceholderExtension(Extension):

    tags = set(['placeholder'])

    def __init__(self, environment):
        super(PhotoPlaceholderExtension, self).__init__(environment)

    def parse(self, parser):
        stream = parser.stream
        lineno = stream.next().lineno

        _args = [parser.parse_expression()]

        if parser.stream.skip_if('comma'):
            _args.append(parser.parse_expression())
        else:
            raise Exception("comma was spected")

        if parser.stream.skip_if('comma'):
            _args.append(parser.parse_expression())
        else:
            _args.append(nodes.Const(None))

        call_node = self.call_method('render', _args)

        return nodes.Output([call_node]).set_lineno(lineno)

    def render(self, width, height, topic):
        if topic:
            return 'http://lorempixel.com/%s/%s/%s' % (width, height, topic)
        else:
            return 'http://lorempixel.com/%s/%s/' % (width, height)
