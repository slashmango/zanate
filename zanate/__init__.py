# -*- coding: utf-8 -*-
import os
import sys
import shutil

from jinja2 import Environment, FileSystemLoader
from zanate.extensions import PhotoPlaceholderExtension

PROJECT_TEMPLATE_DIR = os.path.join(os.path.dirname(__file__),
                                    'project_template')

def delete_output(output_dir):
    try:
        shutil.rmtree(output_dir)
    except:
        pass

def list_templates(template_dir):
    def _check_file(filename):
        file_path = os.path.join(template_dir, filename)
        return os.path.isfile(file_path) and not filename.startswith("_") and not filename.startswith('.')

    return [f for f in os.listdir(template_dir) if _check_file(f)]

def build_env(template_dir, static_url):
    '''returns an environment built from
    the project settings.py file'''
    _globals = {"STATIC_URL": static_url}
    env = Environment(loader=FileSystemLoader(template_dir),
                      extensions=[PhotoPlaceholderExtension])
    env.globals = _globals
    return env

def copy_static(static_dir, static_url, output_dir):
    '''copies the static dir'''
    #TODO: make sure that the static dir name is valid
    if static_url[0] == '/':
        destination_dirname = static_url[1:]
    else:
        destination_dirname = static_url

    destination_dir = os.path.join(output_dir, destination_dirname)
    shutil.copytree(static_dir, destination_dir)

def compile_to(template_name, env, output_dir):
    template = env.get_template(template_name)
    file_name = os.path.join(output_dir, template_name)
    template_file = open(file_name, 'w')
    template_file.write(template.render())
    template_file.close()

def main():
    args = sys.argv
    if len(args) != 2:
        sys.exit("usage: zanate <project_name>")
    else:
        project_name = args[1]

    directory = os.path.normpath(project_name)
    directory = os.path.abspath(directory)

    if os.path.isfile(directory) or os.path.isdir(directory):
        sys.exit("directory of file exists")
    else:
        shutil.copytree(PROJECT_TEMPLATE_DIR, directory)

