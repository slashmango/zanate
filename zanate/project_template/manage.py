import os
import sys
import BaseHTTPServer
import SimpleHTTPServer
from zanate import build_env, copy_static, compile_to, \
                   delete_output, list_templates
from zanate.utils import get_hash_of_dirs


try:
    from manage import Manager
except ImportError:
    sys.exit("usage manage <command> for more information type manage --help")


import settings

manager = Manager()

@manager.command
def build():
    env = build_env(settings.TEMPLATES_DIR, settings.STATIC_URL)

    delete_output(settings.OUTPUT_DIR)
    copy_static(settings.STATIC_DIR, settings.STATIC_URL, settings.OUTPUT_DIR)

    for template in list_templates(settings.TEMPLATES_DIR):
        compile_to(template, env, settings.OUTPUT_DIR)

    print "templates built"

@manager.command
def serve(port=3002):
    current_hash = get_hash_of_dirs([settings.TEMPLATES_DIR,
                                     settings.STATIC_DIR])
    print "BUILDING TEMPLATES"
    build()

    os.chdir(settings.OUTPUT_DIR)
    Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

    httpd = BaseHTTPServer.HTTPServer(("", port), Handler)

    print "serving at port", port
    print "http://0.0.0.0:%s" % port

    while True:
        new_hash = get_hash_of_dirs([settings.TEMPLATES_DIR,
                                     settings.STATIC_DIR])
        if new_hash != current_hash:
            current_hash = new_hash
            print "REBUILDING TEMPLATES"
            build()
            #HACK: needed to add this line, don't know why...
            os.chdir(settings.OUTPUT_DIR)
            continue

        httpd.handle_request()
