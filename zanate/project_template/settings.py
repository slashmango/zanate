import os

PROJECT_DIR = os.path.dirname(__file__)

TEMPLATES_DIR = os.path.join(PROJECT_DIR, 'templates')

STATIC_DIR = os.path.join(PROJECT_DIR, 'static')

STATIC_URL = '/static/'

OUTPUT_DIR = os.path.join(PROJECT_DIR, 'output')
